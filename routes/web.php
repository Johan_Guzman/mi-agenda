<?php

use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\SemanaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//Pagina de Inicio
Route::get('/inicio',[UsuarioController::class,'inicio'])->name('inicio');
//Login y Registro
Route::get('/login',[UsuarioController::class,'login'])->name('login');
Route::get('/registro',[UsuarioController::class,'registro'])->name('registrar');
Route::post('/registrar',[UsuarioController::class,'registroValidar'])->name('registrar.usuario');
Route::post('/registro',[UsuarioController::class,'verficiarUsuario'])->name('validar.usuario');
//Agenda
Route::get('/agenda',[UsuarioController::class,'agenda'])->name('agenda');
//Opciones para el Usuario
Route::get('/verPerfil',[UsuarioController::class,'verPerfil'])->name('ver.perfil');
Route::get('/modificarPerfil',[UsuarioController::class,'modificarPerfil'])->name('modificar.perfil');
Route::post('/modificar',[UsuarioController::class,'modificarUsuario'])->name('modificar.usuario');
Route::get('/eliminarPerfil',[UsuarioController::class,'eliminarPerfil'])->name('eliminar.perfil');
Route::post('/eliminar',[UsuarioController::class,'eliminiarUsuario'])->name('eliminar.usuario');
//Ver Pendientes
Route::get('/lunes',[SemanaController::class,'monday'])->name('lunes');
Route::get('/martes',[SemanaController::class,'tuesday'])->name('martes');
Route::get('/miercoles',[SemanaController::class,'wednesday'])->name('miercoles');
Route::get('/jueves',[SemanaController::class,'thursday'])->name('jueves');
Route::get('/viernes',[SemanaController::class,'friday'])->name('viernes');
Route::get('/sabado',[SemanaController::class,'saturday'])->name('sabado');
Route::get('/domingo',[SemanaController::class,'sunday'])->name('domingo');
//Agregar Pendiente
Route::post('/agregarP',[SemanaController::class,'agregarPendiente'])->name('agregar.pendiente');
//Cerrar Sesion
Route::post('/salir',[UsuarioController::class,'cerrarSesion'])->name('salir');


