<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Mi Agenda</title>
    <!-- CSS -->
    <!-- Bootstrap --><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- DataTables --> <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.css"/>
    <!-- Ajax --> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
    <!-- JS -->
    <!-- Jquery --> <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <!-- DataTables --> <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.23/datatables.min.js"></script>
    <!-- Ajax --> <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/agenda.css') }}">
</head>
<body>
<!-- HEADER & Barra de Navegacion -->
<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{route('agenda')}}">
                <img src="images/logo.png" alt="Logo de Mi Agenda" width="140" height="40" class="d-inline-block align-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav navbar-brand" id="botonesnav">
                <li class="nav-item mr-auto">
                    <a class="nav-link active" aria-current="page" href="{{route('agenda')}}">Hola {{$perfil->nombre}}!</a>
                </li>
                <li class="nav-item mr-auto">
                    <a class="nav-link active" aria-current="page" href=""></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="ScrollingDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false" >
                        <img src="images/perfiles/{{$perfil->ruta}}" id="fotoperfil" class="rounded-circle img-fluid" alt="Foto de Perfil" width="30px" height="30px">
                        Perfil
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarScrollingDropdown">
                        <li><a class="dropdown-item" href="{{route('ver.perfil')}}">Ver Perifl</a></li>
                        <li><a class="dropdown-item" href="{{route('modificar.perfil')}}">Modificar Perfil</a></li>
                        <li><a class="dropdown-item" href="{{route('eliminar.perfil')}}">Eliminar Perfil</a></li>
                    </ul>
                </li>
                <li class="nav-item mr-auto">
                    <a class="nav-link active" aria-current="page" href=""></a>
                </li>
                <li class="nav-item mr-auto">
                    <a class="nav-link active" aria-current="page" href=""></a>
                </li>
                <li class="nav-item mr-auto">
                    <a class="nav-link active" aria-current="page" href="{{route('inicio')}}">Cerrar Sesion</a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- AGENDA -->
<div class="container">
    <div class="col-md-12 row titulo">
        <h1>Pendientes de la Semana </h1>
    </div>
    <br>
    <div class="row modalAgregar" align="right">
        <!-- MODAL -->
        <div class="container">
            <!-- Boton para Abrir el Modal -->
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#agregarLunes">
                Agregar Pendiente
            </button>

            <!-- Modal -->
            <div class="modal fade" id="agregarLunes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Agregar un Pendiente!</h5>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('agregar.pendiente')}}" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12 login-from-row texto">
                                        <label for="pendiente" class="form-label">Pendiente:</label>
                                        <input type="text" class="form-control" id="pendiente" name="pendiente" placeholder="Pendiente" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 login-from-row texto">
                                        <label for="fecha" class="form-label">Fecha:</label>
                                        <input type="date" class="form-control" id="fecha" name="fecha" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 login-from-row texto row horario">
                                        <div class="col-md-6">
                                            <label for="inicioh" class="form-label">Hora de Inicio:</label>
                                            <input type="time" class="form-control" id="inicioh" name="inicioh" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="finh" class="form-label">Hora de Fin:</label>
                                            <input type="time" class="form-control" id="finh" name="finh" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-center">
                                    <div class="col-md-12 login-from-row row botonesmodal">
                                        <div class="col-md-5">
                                            <button type="button" class="btn btn-danger btn-user btn-block" data-bs-dismiss="modal">Cancelar</button>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="submit" class="btn btn-primary btn-user btn-block" value="Registrar">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="col-md-12">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <button class="nav-link active" id="nav-monday-tab" data-bs-toggle="tab" data-bs-target="#nav-monday" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Lunes</button>
                <button class="nav-link" id="nav-tuesday-tab" data-bs-toggle="tab" data-bs-target="#nav-tuesday" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Martes</button>
                <button class="nav-link" id="nav-wednesday-tab" data-bs-toggle="tab" data-bs-target="#nav-wednesday" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Miercoles</button>
                <button class="nav-link" id="nav-thursday-tab" data-bs-toggle="tab" data-bs-target="#nav-thursday" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Jueves</button>
                <button class="nav-link" id="nav-friday-tab" data-bs-toggle="tab" data-bs-target="#nav-friday" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Viernes</button>
                <button class="nav-link" id="nav-saturday-tab" data-bs-toggle="tab" data-bs-target="#nav-saturday" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Sabado</button>
                <button class="nav-link" id="nav-sunday-tab" data-bs-toggle="tab" data-bs-target="#nav-sunday" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Domingo</button>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-monday" role="tabpanel" aria-labelledby="nav-home-tab"><br>
                <!--Tabla de Agenda de Lunes -->
                <table class="table table-striped" style="width:100%" id="tableMonday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-tuesday" role="tabpanel" aria-labelledby="nav-profile-tab"><br>
                <!--Tabla de Agenda de Martes -->
                <table class="table table-striped" style="width:100%" id="tableTuesday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-wednesday" role="tabpanel" aria-labelledby="nav-contact-tab"><br>
                <!--Tabla de Agenda de Miercoles -->
                <table class="table table-striped" style="width:100%" id="tableWednesday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-thursday" role="tabpanel" aria-labelledby="nav-contact-tab"><br>
                <!--Tabla de Agenda de Jueves -->
                <table class="table table-striped" style="width:100%" id="tableThursday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-friday" role="tabpanel" aria-labelledby="nav-profile-tab"><br>
                <!--Tabla de Agenda de Viernes -->
                <table class="table table-striped" style="width:100%" id="tableFriday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-saturday" role="tabpanel" aria-labelledby="nav-contact-tab"><br>
                <!--Tabla de Agenda de Sabado -->
                <table class="table table-striped" style="width:100%" id="tableSaturday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-sunday" role="tabpanel" aria-labelledby="nav-contact-tab"><br>
                <!--Tabla de Agenda de Domingo -->
                <table class="table table-striped" style="width:100%" id="tableSunday">
                    <thead>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Pendiente</th>
                        <th>Mes</th>
                        <th>Dia</th>
                        <th>Hora de Incio</th>
                        <th>Hora de Fin</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(document).ready(function() {
        $('#tableMonday').DataTable({
            "processing":true,
            "ajax":"{{ route('lunes')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
        $('#tableTuesday').DataTable({
            "processing":true,
            "ajax":"{{ route('martes')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
        $('#tableWednesday').DataTable({
            "processing":true,
            "ajax":"{{ route('miercoles')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
        $('#tableThursday').DataTable({
            "processing":true,
            "ajax":"{{ route('jueves')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
        $('#tableFriday').DataTable({
            "processing":true,
            "ajax":"{{ route('viernes')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
        $('#tableSaturday').DataTable({
            "processing":true,
            "ajax":"{{ route('sabado')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
        $('#tableSunday').DataTable({
            "processing":true,
            "ajax":"{{ route('domingo')}}",
            "columns":[
                {data: 'pendiente'},
                {data: 'mes'},
                {data: 'dia'},
                {data: 'inicioh'},
                {data: 'finh'}
            ]
        });
    });
</script>
</body>
</html>
