<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Login</title>
    <!-- CSS -->
    <!-- Bootstrap --> <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <!-- Ajax --> <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />

    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
</head>
<body>
<div class="container">
    <div class="row text-center login-page">
        <div class="col-md-12 login-form">
            <form action="{{route('validar.usuario')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-md-12 login-form-header">
                        <p class="login-form-font-header">Mi<span>Agenda</span><p>
                        <p class="login-form-font-header2">Inicia Sesion<p>
                    </div>
                    @if(isset($mensaje))
                        @if($estatus == "error")
                            <label class="text-danger">{{$mensaje}}</label>
                        @else
                            <label class="text-success">{{$mensaje}}</label>
                        @endif
                    @endif
                </div>
                <div class="row">
                    <div class="col-md-12 login-from-row texto">
                        <label for="email" class="form-label">Correo</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="alguien@example.com">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 login-from-row texto">
                        <label for="password" class="form-label ">Contraseña</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 login-from-row">
                        <input type="submit" class="btn btn-primary btn-user btn-block" value="Iniciar Sesion">
                    </div>
                </div>
                <div class="text-center">
                    <div class="col-md-12 login-from-row">
                        <p id="registrate">¿No tienes una cuenta? <a class="small" href="{{route('registrar')}}">Registrate</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- JS -->
<!-- Jquery --> <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<!-- Bootstrap --> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
<!-- Ajax --> <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
</body>
</html>
