<?php

namespace App\Http\Controllers;

use App\Models\Semana;
use App\Http\Controllers\UsuarioController;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class SemanaController extends Controller
{
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "LUNES"
    public function monday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $lunes = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Lunes")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($lunes)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "MARTES"
    public function tuesday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $martes = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Martes")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($martes)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "MIERCOLES"
    public function wednesday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $miercoles = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Miercoles")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($miercoles)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "JUEVES"
    public function thursday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $jueves = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Jueves")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($jueves)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "VIERNES"
    public function friday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $viernes = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Viernes")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($viernes)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "SABADO"
    public function saturday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $sabado = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Sabado")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($sabado)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION OBTENER REGISTROS "DOMINGO"
    public function sunday(Request $request){
        session_start();
        $correo = $_SESSION['perfil'];
        if($request->ajax()){
            $domingo = Semana::select("pendiente", "mes", "dia", "inicioh", "finh")->where("semana", "Domingo")->where("perfil", $correo)
                ->orderBy('mes','DESC')->orderBy('dia','DESC')->orderBy('inicioh','ASC')->get();
            return DataTables::of($domingo)
                ->make(true);
        }
        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION PARA CAMBIAR EL MES A ESPAñOL
    function meses($mes){
        $mesf = null;
        if($mes == "January"){
             $mesf = "Enero";
        }
        if($mes == "February"){
            $mesf = "Febrero";
        }
        if($mes == "March"){
            $mesf = "Marzo";
        }
        if($mes == "April"){
            $mesf = "Abril";
        }
        if($mes == "May"){
            $mesf = "Mayo";
        }
        if($mes == "June"){
            $mesf = "Junio";
        }
        if($mes == "July"){
            $mesf = "Julio";
        }
        if($mes == "August"){
            $mesf = "Agosto";
        }
        if($mes == "September"){
            $mesf = "Septiembre";
        }
        if($mes == "October"){
            $mesf = "Octubre";
        }
        if($mes == "November"){
            $mesf = "Noviembre";
        }
        if($mes == "December"){
            $mesf = "Diciembre";
        }
        return $mesf;
    }
    // ------------------------------------------------------------- FUNCION PARA CAMBIAR EL MES A ESPAñOL
    function semanas($semana){
        $semanaf = null;
        if($semana == "1"){
            $semanaf = "Lunes";
        }
        if($semana == "2"){
            $semanaf = "Martes";
        }
        if($semana == "3"){
            $semanaf = "Miercoles";
        }
        if($semana == "4"){
            $semanaf = "Jueves";
        }
        if($semana == "5"){
            $semanaf = "Viernes";
        }
        if($semana == "6"){
            $semanaf = "Sabado";
        }
        if($semana == "7"){
            $semanaf = "Domingo";
        }
        return $semanaf;
    }
    // ------------------------------------------------------------- FUNCION PARA CAMBIAR EL MES A ESPAñOL
    function agregar($pendiente, $mesf, $semanaf, $dia, $inicioh, $finh, $correo){
        $semana = new Semana();
        $semana->pendiente = $pendiente;
        $semana->mes = $mesf;
        $semana->semana = $semanaf;
        $semana->dia = $dia;
        $semana->inicioh = $inicioh;
        $semana->finh = $finh;
        $semana->perfil = $correo;
        $semana->save();
    }
    // ------------------------------------------------------------- FUNCION AGREGAR REGISTROS "LUNES"
    public function agregarPendiente(Request $pendientes){
        $pendiente = $pendientes->pendiente;
        $fecha = $pendientes->fecha;
        $horasi = $pendientes->inicioh;
        $horasf = $pendientes->finh;
        $dia = date('d', strtotime($fecha));
        $mes = date('F', strtotime($fecha));
        $semana = date('N', strtotime($fecha));
        $mesf = $this->meses($mes);
        $semanaf = $this->semanas($semana);
        $horai = date('g', strtotime($horasi));
        $minutosi = date('i', strtotime($horasi));
        $meridianoi = date('A', strtotime($horasi));
        $meridianof = date('A', strtotime($horasf));
        $horaf = date('g', strtotime($horasf));
        $minutosf = date('i', strtotime($horasf));
        $inicioh = $horai.":".$minutosi." ".$meridianoi;
        $finh = $horaf.":".$minutosf." ".$meridianof;
        session_start();
        $correo = $_SESSION['perfil'];
        $this->agregar($pendiente, $mesf, $semanaf, $dia, $inicioh, $finh, $correo);
        return redirect()->route('agenda');
    }

}
