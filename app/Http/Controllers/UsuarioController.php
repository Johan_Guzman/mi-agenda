<?php

namespace App\Http\Controllers;

use App\Models\Semana;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use SplFileInfo;

class UsuarioController extends Controller
{
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL INICIO
    public function inicio(){
        return view("index");
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL LOGIN
    public function login(){
        return view("login");
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR AL REGISTRO
    public function registro(){
        return view("registro");
    }
        // ------------------------------------------------------------- FUNCION PARA AGREGAR UN NUEVO USUARIO
    public function registroValidar(Request $informacion){
        //Valida que se hayan llenado todos los datos
        if(!$informacion->name || !$informacion->lastname || !$informacion->email || !$informacion->confemail ||
            !$informacion->password || !$informacion->confpassword){
            return view("registro",["estatus"=> "error", "mensaje"=> "¡Complete todos los campos Obligatorios!"]);
        }
        //Valida que el correo no exista en la base de datos
        $usuario = Usuario::where('correo',$informacion->email)->first();
        if($usuario == true){
            return view("registro",["estatus"=> "error", "mensaje"=> "Ya existe una cuenta con el Correo Ingresado"]);
        }
        //Parte de Imagen del usuario
        $nombreImagen = null;
        //extraemos la imagen si existe
        if($informacion->hasFile('foto')){
            //Se genera e nombre al azar
            $letras = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z');
            $letrasM = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z');
            $numeros = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
            //FOTO
            $file = $informacion->file('foto'); //La foto se almacena en una varaible
            //El nombre de la imagen constara de 30 caracteres, y se validara que no sea el uno ya registrado
            do{
                for($x = 0; $x<10; $x++){
                    $nombreImagen = $nombreImagen.$letras[array_rand($letras)];//Asignamos una letra minuscula al azar
                    $nombreImagen = $nombreImagen.$letrasM[array_rand($letrasM)];//Asignamos una letra mayuscula al azar
                    $nombreImagen = $nombreImagen.$numeros[array_rand($numeros)];//Asignamos un numero al azar
                }
                $extension = $file->getClientOriginalExtension();//Extraemos la extension de la foto
                if($extension != "png" && $extension != "jpg"){//validamos que sea una foto y no otro archivo
                    return view("registro",["estatus"=> "error", "mensaje"=> "La foto debe de ser jpg o png"]);
                }
                $nombreImagen = $nombreImagen.".".$extension;//agregamos la extension de la foto al nombre creado
            }while(Usuario::where('ruta',$nombreImagen)->first());
            $file->move(public_path().'/images/perfiles/', $nombreImagen);//movemos la foto a la carpeta images en public
        }
        //si no existe, se definira como "perfil" que e suna imagen general
        if($nombreImagen == null){
            $nombreImagen = "perfil.png";
        }
        //Se extraen todas las variables del formulario
        $nombre = $informacion->name;
        $apellido = $informacion->lastname;
        $ruta = $nombreImagen;
        $correo = $informacion->email;
        $confcorreo = $informacion->confemail;
        $password = $informacion->password;
        $confpassword = $informacion->confpassword;
        //Se valida que el correo sea el mismo (confirmacion)
        if(($correo != $confcorreo)){
            return view("registro",["estatus"=> "error", "mensaje"=> "Los Correos No Coinciden"]);
        }
        //Se valida que la contraseña sea el mismo (confirmacion)
        if(($password != $confpassword)){
            return view("registro",["estatus"=> "error", "mensaje"=> "Las Contraseñas no Coinciden"]);
        }
        //Se sube la informacion a DB
        $usuario = new Usuario();
        $usuario->nombre =  $nombre;
        $usuario->apellido =  $apellido;
        $usuario->ruta =  $ruta;
        $usuario->correo =  $correo;
        $usuario->password = bcrypt($password);
        $usuario->save();
        return view("login",["estatus"=> "aprobado", "mensaje"=> "¡Cuenta Creada!"]);
    }
    // ------------------------------------------------------------- FUNCION PARA VERIFICAR A UN USUARIO
    public function verficiarUsuario(Request $informacion){
        if(!$informacion->email || !$informacion->password){
            return view("login",["estatus"=> "error", "mensaje"=> "¡Completa los campos!"]);
        }
        $usuario = Usuario::where("correo",$informacion->email)->first();
        if(!$usuario){
            return view("login",["estatus"=> "error", "mensaje"=> "¡El correo no esta registrado!"]);
        }
        if(!Hash::check($informacion->password,$usuario->password)){
            return view("login",["estatus"=> "error", "mensaje"=> "¡Datos incorrectos!"]);
        }
        Session::put('usuario',$usuario);
        session_start();
        $_SESSION['perfil'] = $informacion->email;

        return redirect()->route('agenda');
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR A LA AGENDA
    public function agenda(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        return view('agenda', compact('perfil'));
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR A LA VISTA DEL PERFIL
    public function verPerfil(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        return view("ver", compact('perfil'));
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR A LA VISTA DE MODIFICAR
    public function modificarPerfil(){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        return view("modificar", compact('perfil'));
    }
    // ------------------------------------------------------------- FUNCION PARA MODIFICAR A UN USUARIO
    public function modificarUsuario(Request $informacion){
        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        if(!$informacion->passwordactual){
            return view("modificar",["estatus"=> "error", "mensaje"=> "Debe de Escribir su contraseña actual para confirmar"], compact('perfil'));
        }
        if(!Hash::check($informacion->passwordactual,$perfil->password)){
            return view("modificar",["estatus"=> "error", "mensaje"=> "¡Contraseña Incorrecta!"], compact('perfil'));
        }
        $nombre = $informacion->name;
        $apellido = $informacion->lastname;
        //$ruta = $nombreImagen;
        $correo = $informacion->email;
        $confcorreo = $informacion->confemail;
        $password = $informacion->password;
        $confpassword = $informacion->confpassword;
                //Se valida que el correo sea el mismo (confirmacion)
        if(($correo != $confcorreo)){
            return view("modificar",["estatus"=> "error", "mensaje"=> "Los Correos nuevos No Coinciden"]);
        }
        //Se valida que la contraseña sea el mismo (confirmacion)
        if(($password != $confpassword)){
            return view("modificar",["estatus"=> "error", "mensaje"=> "Las Contraseñas nuevas No Coinciden"]);
        }
        $usuario = Usuario::findOrFail($perfil->id);
        if($nombre != null || $nombre != ""){
            $usuario->nombre = $nombre;
        }
        if($apellido != null || $apellido != ""){
            $usuario->apellido = $apellido;
        }
        $nombreImagen = null;
        if($informacion->hasFile('foto')){
            //Se genera e nombre al azar
            $letras = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                'u', 'v', 'w', 'x', 'y', 'z');
            $letrasM = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
                'U', 'V', 'W', 'X', 'Y', 'Z');
            $numeros = array('1', '2', '3', '4', '5', '6', '7', '8', '9');
            //FOTO
            $file = $informacion->file('foto'); //La foto se almacena en una varaible
            //El nombre de la imagen constara de 30 caracteres, y se validara que no sea el uno ya registrado
            do{
                for($x = 0; $x<10; $x++){
                    $nombreImagen = $nombreImagen.$letras[array_rand($letras)];//Asignamos una letra minuscula al azar
                    $nombreImagen = $nombreImagen.$letrasM[array_rand($letrasM)];//Asignamos una letra mayuscula al azar
                    $nombreImagen = $nombreImagen.$numeros[array_rand($numeros)];//Asignamos un numero al azar
                }
                $extension = $file->getClientOriginalExtension();//Extraemos la extension de la foto
                if($extension != "png" && $extension != "jpg"){//validamos que sea una foto y no otro archivo
                    return view("modificar",["estatus"=> "error", "mensaje"=> "La foto debe de ser jpg o png"], compact('perfil'));
                }
                $nombreImagen = $nombreImagen.".".$extension;//agregamos la extension de la foto al nombre creado
            }while(Usuario::where('ruta',$nombreImagen)->first());
            //Elimina el archivo de la foto que tenga el usuario
            $fotov = $perfil->ruta;
            $fotoactual = public_path().'/images/perfiles/'.$fotov;
            if($fotov != "perfil.png"){
                unlink($fotoactual);
            }
            $file->move(public_path().'/images/perfiles/', $nombreImagen); //movemos la foto a la carpeta images en public
            $usuario->ruta = $nombreImagen;
        }
        if($correo != null || $correo != ""){
            $usuario->correo = $correo;
        }
        if($password != null || $password != ""){
            $usuario->$password = $password;
        }
        $usuario->save();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        return view("ver",["estatus"=> "bien", "mensaje"=> "Su perfil se ha Actualizado Correctamente"], compact('perfil'));
    }
    // ------------------------------------------------------------- FUNCION PARA INGRESAR A LA LISTA DE ELIMINAR
    public function eliminarPerfil(){
        return view('eliminar');
    }
    // ------------------------------------------------------------- FUNCION PARA ELIMINAR A UN USUARIO
    public function  eliminiarUsuario(){

        session_start();
        $perfil = Usuario::where("correo",$_SESSION['perfil'])->first();
        $usuario = Usuario::find($perfil->id);
        $semanas = Semana::where("perfil",$_SESSION['perfil'])->first();
        if(!$semanas == null){
            while(!$semanas == null){
                $tareas = Semana::find($semanas->id);
                $tareas->delete();
                $semanas = Semana::where("perfil", $_SESSION['perfil'])->first();
            }
        }
        $fotov = $perfil->ruta;
        $fotoactual = public_path().'/images/perfiles/'.$fotov;
        if($fotov != "perfil.png"){
            unlink($fotoactual);
        }
        $usuario->delete();
        unset($_SESSION['perfil']);
        return redirect()->route('inicio');
    }
    // ------------------------------------------------------------- FUNCION PARA CERRAR SESION
    public function cerrarSesion(){
        unset($_SESSION['perfil']);
        view('index');
    }
}
