<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Semana extends Model
{
    protected $table = "semanas";
    protected $fillable = [
      'pendiente', 'mes', 'semana', 'dia', 'inicioh', 'finh'
    ];
    public $timestamps = false;
}
